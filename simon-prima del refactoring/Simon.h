/*
  Simon.h
  Created by Dario, October 31, 2016.
  Released into the public domain.
*/
#ifndef Simon_h
#define Simon_h

#include "Arduino.h"

class Simon
{
  public:
    void begin(int ledPins[], int soundPin);
    void playSequence();
    boolean checkValue(int index, int value);
    void gameOver();
    void play(int value);
    boolean sequenceCompleted(int current);
    void silence();
    void resetLights();
  private:
    int _ledPins[4];
    int _soundPin;
    int mapValueToPin(int value);
    int mapValueToNote(int value);
    void nextLevel();
    void flashLevelPassed();
    void flashGameOver();
    void stopPlaying(int value);
};
#endif
