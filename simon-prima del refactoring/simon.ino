#include "Simon.h"

const int SIMON = 0;
const int PLAYER = 1;

int turn = SIMON;
int buttonPins[4];

int currentIndex = 0;
int currentValue = -1;

Simon simon;

void setup() {
  int ledPins[4];
  int soundPin = 6;

  for(int i=0;i<4;i++){
    ledPins[i]=i+8;
    pinMode(ledPins[i], OUTPUT);
  }
  for(int i=0;i<4;i++){
    buttonPins[i] = i+2;
    pinMode(buttonPins[i], INPUT);
  }
  pinMode(soundPin,OUTPUT);

  simon.begin(ledPins, soundPin);
  
  Serial.begin(9600);
  Serial.println("setup");
}

void loop() {
  if(turn == SIMON){
    simon.playSequence();
    currentIndex = 0;
    currentValue = -1;
    turn = PLAYER;
  } else {
    simon.resetLights(); // spostare in readValue?
    int val = readValue();
    if( val >= 0) {
      simon.play(val); // spostare in readValue?
      currentValue = val;
    } else {
      simon.silence(); // spostare in readValue?
      if(currentValue >= 0){
        logData(currentValue, currentIndex);
        if(simon.checkValue(currentIndex, currentValue)){
          Serial.println(">>> beccato");
          currentValue = -1;
          currentIndex++;
          if(simon.sequenceCompleted(currentIndex)){
            Serial.println("play next sequence");
            turn = SIMON;
          }
        } else {
          Serial.println("====================================");
          logData(currentValue, currentIndex);
          simon.gameOver();
          turn = SIMON;
        }
      }
    }
    delay(10);
  }
}


int readValue(){
  
  int val = -1;
  for(int i=0;i<4;i++){
    if(digitalRead(buttonPins[i]) == HIGH){
      val = i;
      break;
    }
  }
  
//  simon.resetLights(); // spostare fuori prima di readValue?
//  if(val >= 0){
//    simon.play(val); // spostare fuori insieme a currentVal = val?
//  }
  return val;
}

void logData(int currentValue, int currentIndex){
  Serial.print("currentValue ==");
  Serial.println(currentValue);
  Serial.print("currentIndex ==");
  Serial.println(currentIndex);
  Serial.print("data[currentIndex] ==");
//  Serial.println(sequence.get(currentIndex));
}

