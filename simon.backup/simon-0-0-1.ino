const int PLAY = 0;
const int READ = 1;
const int SIZE = 10;

int mode = PLAY;
int data[SIZE];
int index = 0;
int timerValue = 1000;
int current = 0;
boolean consumed = false;

void setup() {
  // put your setup code here, to run once:
  for(int i=6;i<13;i+=2){
    pinMode(i, OUTPUT);
  }
  pinMode(A0, INPUT);
  Serial.begin(9600);
  randomSeed(analogRead(5));
  Serial.println("setup");
}

void loop() {
  // put your main code here, to run repeatedly:
  if(mode == PLAY){
    playSequence();
    current = 0;
    consumed = false;
  } else {
    int val = readValue();
    if( val >= 0) {
      if(!consumed){
        Serial.print("val ==");
        Serial.println(val);
        Serial.print("current ==");
        Serial.println(current);
        Serial.print("data[current] ==");
        Serial.println(data[current]);
        if(val == data[current]){
          Serial.println("beccato");
          consumed = true;
          val = -1;
          current++;
          if(current >= index){
            Serial.println("play next sequence");
            mode = PLAY;
          }
        } else {
          Serial.println("====================================");
          Serial.print("val ==");
          Serial.println(val);
          Serial.print("current ==");
          Serial.println(current);
          Serial.print("data[current] ==");
          Serial.println(data[current]);
          gameOver();
        }
      }
    } else {
      consumed = false;
    }
  }
  delay(10);
}

void playSequence(){
  Serial.println("playSequence");
  Serial.print("index == ");
  Serial.println(index);

  if(index == SIZE){
    increaseDifficulty();
    index = 0;
  }
  data[index++] = random(0, 4);
  delay(timerValue);
  for(int j=6;j<13;j+=2){
    digitalWrite(j, LOW);
  }
  delay(timerValue);
  for(int i = 0; i<index; i++){
    digitalWrite(mapValueToPin(data[i]), HIGH);
    Serial.print(data[i]);
    Serial.print("-");
    delay(timerValue);
    digitalWrite(mapValueToPin(data[i]), LOW);
    delay(timerValue);
  }
  Serial.println("X");
  mode = READ;
}

int mapValueToPin(int value){
  return 12 - (value * 2);
}

void increaseDifficulty(){
  Serial.println("increaseDifficulty");
  Serial.print("timerValue == ");
  Serial.println(timerValue);
  if(timerValue > 0){
    timerValue = timerValue / 2;
  }
}

int readValue(){
  int keyVal = analogRead(A0);
//  Serial.println(keyVal);
  int val = -1;
  if(keyVal == 1023){
      val = 0;
  } else if(keyVal >= 990 && keyVal <= 1010){
    val = 1;
  } else if(keyVal >= 505 && keyVal <= 515){
    val = 2;
  } else if(keyVal >= 5 && keyVal <= 12){
    val = 3;
  }

  for(int i=6;i<13;i+=2){
    digitalWrite(i, LOW);
  }
  if(val >= 0){
    digitalWrite(mapValueToPin(val), HIGH);
  }
//  Serial.print(val);
//  Serial.print("-");
  return val;
}

void gameOver(){
  Serial.println("Game Over");
  mode = PLAY;
  index = 0;
  timerValue = 1000;
}

