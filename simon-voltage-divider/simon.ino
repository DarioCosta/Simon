#include <LinkedList.h>

const int SIMON = 0;
const int PLAYER = 1;
const int SIZE = 5;
const int TIME = 1000;

int turn = SIMON;
int size = SIZE;
LinkedList<int> sequence = LinkedList<int>();

int index = 0;
int timerValue = TIME;
int currentIndex = 0;
int currentValue = -1;

void setup() {
  for(int i=6;i<13;i+=2){
    pinMode(i, OUTPUT);
  }
  pinMode(A0, INPUT);
  Serial.begin(9600);
  randomSeed(analogRead(5));
  Serial.println("setup");
}

void loop() {
  if(turn == SIMON){
    playSequence();
    currentIndex = 0;
    currentValue = -1;
    turn = PLAYER;
  } else {
    int val = readValue();
    if( val >= 0) {
      currentValue = val;
    } else {
      if(currentValue >= 0){
        logData(currentValue, currentIndex);
        if(currentValue == sequence.get(currentIndex)){
          Serial.println(">>> beccato");
          currentValue = -1;
          currentIndex++;
          if(currentIndex >= index){
            Serial.println("play next sequence");
            turn = SIMON;
          }
        } else {
          Serial.println("====================================");
          logData(currentValue, currentIndex);
          gameOver();
        }
      }
    }
    delay(10);
  }
}

void playSequence(){
  Serial.println("---------------- playSequence -----------------");
  Serial.print("index == ");
  Serial.println(index);

  if(index == size){
    nextLevel();
    index = 0;
    sequence.clear();
  }
  sequence.add(random(0,4));
  index++;
  resetLights();
  for(int i = 0; i<index; i++){
    delay(timerValue);
    digitalWrite(mapValueToPin(sequence.get(i)), HIGH);
    Serial.print(sequence.get(i));
    Serial.print("-");
    delay(timerValue);
    digitalWrite(mapValueToPin(sequence.get(i)), LOW);
  }
  Serial.println();
  Serial.println("-----------------------------");
}

int mapValueToPin(int value){
  return 12 - (value * 2);
}

void nextLevel(){
  Serial.println("increaseDifficulty");
  Serial.print("timerValue == ");
  Serial.println(timerValue);
  flashLevelPassed();
  if(timerValue > 0){
    timerValue = timerValue / 2;
  }
  size = size + 5;
}

int readValue(){
  int keyVal = analogRead(A0);
//  Serial.println(keyVal);
  int val = -1;
  if(keyVal == 1023){
      val = 0;
  } else if(keyVal >= 990 && keyVal <= 1010){
    val = 1;
  } else if(keyVal >= 505 && keyVal <= 515){
    val = 2;
  } else if(keyVal >= 5 && keyVal <= 12){
    val = 3;
  }

  resetLights();
  if(val >= 0){
    digitalWrite(mapValueToPin(val), HIGH);
  }
  return val;
}

void resetLights(){
  for(int i=6;i<13;i+=2){
    digitalWrite(i, LOW);
  }
}
void gameOver(){
  Serial.println("================== Game Over ==================");
  flashGameOver();
  turn = SIMON;
  index = 0;
  timerValue = TIME;
  size = SIZE;
  sequence.clear();
}

void flashGameOver(){
  for(int i=0;i<5;i++){
    for(int j=6;j<13;j+=2){
      digitalWrite(j, HIGH);
    }
    delay(200);
    resetLights();
    delay(200);
  }
}

void flashLevelPassed(){
  for(int x=0;x<5;x++){
    resetLights();
    for(int i=6;i<13;i+=2){
      digitalWrite(i, HIGH);
      delay(100);
      digitalWrite(i, LOW);
    }
  }
}

void logData(int currentValue, int currentIndex){
  Serial.print("currentValue ==");
  Serial.println(currentValue);
  Serial.print("currentIndex ==");
  Serial.println(currentIndex);
  Serial.print("data[currentIndex] ==");
  Serial.println(sequence.get(currentIndex));
}

