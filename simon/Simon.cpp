/*
  Simon.cpp
  Created by Dario, October 31, 2016.
  Released into the public domain.
*/

#include "Arduino.h"
#include "Simon.h"
#include <LinkedList.h>

LinkedList<int> sequence = LinkedList<int>();

const int SIZE = 5;
const int TIME = 800;

int notes[4];
int index = 0;
int timerValue = TIME;
int size = SIZE;

void Simon::begin(int ledPins[], int soundPin)
{
  for(int i=0;i<4; i++){
    _ledPins[i] = ledPins[i];
  }
  _soundPin = soundPin;
  notes[0] = 262;
  notes[1] = 330;
  notes[2]= 392;
  notes[3] = 523;
  randomSeed(analogRead(5)); // parametrizzare?
}

void Simon::playSequence(){
  Serial.println("---------------- playSequence -----------------");
  Serial.print("index == ");
  Serial.println(index);

  if(index == size){
    nextLevel();
    index = 0;
    sequence.clear();
  }
  sequence.add(random(0,4));
  index++;
  resetLights();
  for(int i = 0; i<index; i++){
    delay(timerValue);
    play(sequence.get(i));
    Serial.print(sequence.get(i));
    Serial.print("-");
    delay(timerValue);
    stopPlaying(sequence.get(i));
  }
  Serial.println();
  Serial.println("-----------------------------");
}

void Simon::nextLevel(){
  Serial.println("increaseDifficulty");
  Serial.print("timerValue == ");
  Serial.println(timerValue);
  flashLevelPassed();
  if(timerValue > 0){
    timerValue = timerValue / 2;
  }
  size = size + 5;
}

boolean Simon::isSequenceCompleted(int current){
  return current >= index;  
}

boolean Simon::checkValue(int index, int value)
{
  return value == sequence.get(index);
}

void Simon::resetLights(){
  for(int i=0;i<4;i++){
    digitalWrite(_ledPins[i], LOW);
  }
}

void Simon::gameOver(){
  Serial.println("================== Game Over ==================");
  flashGameOver();
  index = 0;
  timerValue = TIME;
  size = SIZE;
  sequence.clear();
}


void Simon::play(int value){
    digitalWrite(mapValueToPin(value), HIGH);
    tone(_soundPin, mapValueToNote(value));
}

void Simon::stopPlaying(int value){
    digitalWrite(mapValueToPin(value), LOW);
    noTone(_soundPin);
}

void Simon::silence(){
    noTone(_soundPin);
}

int Simon::mapValueToPin(int value){
  return _ledPins[value];
}
int Simon::mapValueToNote(int value){
  return notes[value];
}
void Simon::flashGameOver(){
  tone(_soundPin, 0);
  for(int i=0;i<5;i++){
    for(int j=0;j<4;j++){
      digitalWrite(_ledPins[j], HIGH);
    }
    delay(200);
    resetLights();
    delay(200);
  }
  noTone(_soundPin);
}

void Simon::flashLevelPassed(){
  int melody[] = {262, 196, 196, 220, 196, 0, 247, 262};
  int noteDurations[] = {4,8,8,4,4,4,4,4};
  
  for(int i=0;i<8;i++){
    digitalWrite(_ledPins[melody[i]%4], HIGH);
    tone(_soundPin, melody[i]);
    delay(1200/noteDurations[i]);
    digitalWrite(_ledPins[melody[i]%4], LOW);
    noTone(_soundPin);
    delay(5);
  }
  
  for(int x=0;x<5;x++){
    resetLights();
    for(int i=0;i<4;i++){
      digitalWrite(_ledPins[i], HIGH);
      delay(100);
      digitalWrite(_ledPins[i], LOW);
    }
  }
}

