#include "Simon.h"

const int SIMON = 0;
const int PLAYER = 1;

int turn = SIMON;
int buttonPins[4];

int currentIndex = 0;
int currentValue;

Simon simon;

void setup() {
  int ledPins[4];
  int soundPin = 6;

  for(int i=0;i<4;i++){
    ledPins[i]=i+8;
    pinMode(ledPins[i], OUTPUT);
  }
  for(int i=0;i<4;i++){
    buttonPins[i] = i+2;
    pinMode(buttonPins[i], INPUT);
  }
  pinMode(soundPin,OUTPUT);

  simon.begin(ledPins, soundPin);
  currentValue = simon.NO_VALUE;
  
  Serial.begin(9600);
  Serial.println("setup");
}

void loop() {
  if(turn == SIMON){
    simon.playSequence();
    currentIndex = 0;
    currentValue = simon.NO_VALUE;
    turn = PLAYER;
  } else {
    //simon.resetLights(); // spostare in readValue?
    int val = readValue();
    if( val != simon.NO_VALUE) {
      //simon.play(val); // spostare in readValue?
      currentValue = val;
    } else {
      //simon.silence(); // spostare in readValue?
      if(currentValue != simon.NO_VALUE){
        logData(currentValue, currentIndex);
        if(simon.checkValue(currentIndex, currentValue)){
          Serial.println(">>> beccato");
          currentValue = simon.NO_VALUE;
          currentIndex++;
          if(simon.isSequenceCompleted(currentIndex)){
            Serial.println("play next sequence");
            turn = SIMON;
          }
        } else {
          Serial.println("====================================");
          logData(currentValue, currentIndex);
          simon.gameOver();
          turn = SIMON;
        }
      }
    }
    delay(10);
  }
}


int readValue(){
  
  int val = simon.NO_VALUE;
  for(int i=0;i<4;i++){
    if(digitalRead(buttonPins[i]) == HIGH){
      val = i;
      break;
    }
  }
  
  simon.resetLights(); // spostare fuori prima di readValue?
  if(val != simon.NO_VALUE){
    simon.play(val); // spostare fuori insieme a currentVal = val?
  } else {
    simon.silence();
  }
  return val;
}

void logData(int currentValue, int currentIndex){
  Serial.print("currentValue ==");
  Serial.println(currentValue);
  Serial.print("currentIndex ==");
  Serial.println(currentIndex);
  Serial.print("data[currentIndex] ==");
//  Serial.println(sequence.get(currentIndex));
}

