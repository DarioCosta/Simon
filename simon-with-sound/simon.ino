#include <LinkedList.h>

const int SIMON = 0;
const int PLAYER = 1;
const int SIZE = 5;
const int TIME = 800;

int turn = SIMON;
int size = SIZE;

LinkedList<int> sequence = LinkedList<int>();
int ledPins[4];
int buttonPins[4];
int soundPin = 6;
int notes[4];

int index = 0;
int timerValue = TIME;
int currentIndex = 0;
int currentValue = -1;

void setup() {
  for(int i=0;i<4;i++){
    ledPins[i]=i+8;
    pinMode(ledPins[i], OUTPUT);
  }
  for(int i=0;i<4;i++){
    buttonPins[i] = i+2;
    pinMode(buttonPins[i], INPUT);
  }
  pinMode(soundPin,OUTPUT);
  notes[0] = 262;
  notes[1] = 330;
  notes[2]= 392;
  notes[3] = 523;

  Serial.begin(9600);
  randomSeed(analogRead(5));
  Serial.println("setup");
}

void loop() {
  if(turn == SIMON){
    playSequence();
    currentIndex = 0;
    currentValue = -1;
    turn = PLAYER;
  } else {
    int val = readValue();
    if( val >= 0) {
      currentValue = val;
    } else {
      noTone(soundPin);
      if(currentValue >= 0){
        logData(currentValue, currentIndex);
        if(currentValue == sequence.get(currentIndex)){
          Serial.println(">>> beccato");
          currentValue = -1;
          currentIndex++;
          if(currentIndex >= index){
            Serial.println("play next sequence");
            turn = SIMON;
          }
        } else {
          Serial.println("====================================");
          logData(currentValue, currentIndex);
          gameOver();
        }
      }
    }
    delay(10);
  }
}

void playSequence(){
  Serial.println("---------------- playSequence -----------------");
  Serial.print("index == ");
  Serial.println(index);

  if(index == size){
    nextLevel();
    index = 0;
    sequence.clear();
  }
  sequence.add(random(0,4));
  index++;
  resetLights();
  for(int i = 0; i<index; i++){
    delay(timerValue);
    playValue(sequence.get(i));
    delay(timerValue);
    stopPlaying(sequence.get(i));
  }
  Serial.println();
  Serial.println("-----------------------------");
}

void playValue(int value){
    digitalWrite(mapValueToPin(value), HIGH);
    tone(soundPin, mapValueToNote(value));
    Serial.print(value);
    Serial.print("-");
}

int mapValueToPin(int value){
  return ledPins[value];
}

int mapValueToNote(int value){
  return notes[value];
}

void stopPlaying(int value){
    digitalWrite(mapValueToPin(value), LOW);
    noTone(soundPin);
}

void nextLevel(){
  Serial.println("increaseDifficulty");
  Serial.print("timerValue == ");
  Serial.println(timerValue);
  flashLevelPassed();
  if(timerValue > 0){
    timerValue = timerValue / 2;
  }
  size = size + 5;
}

int readValue(){
  
  int val = -1;
  for(int i=0;i<4;i++){
    if(digitalRead(buttonPins[i]) == HIGH){
      val = i;
      break;
    }
  }
  
  resetLights();
  if(val >= 0){
    playValue(val);
  }
  return val;
}

void resetLights(){
  for(int i=0;i<4;i++){
    digitalWrite(ledPins[i], LOW);
  }
}
void gameOver(){
  Serial.println("================== Game Over ==================");
  flashGameOver();
  turn = SIMON;
  index = 0;
  timerValue = TIME;
  size = SIZE;
  sequence.clear();
}

void flashGameOver(){
  for(int i=0;i<5;i++){
    for(int j=0;j<4;j++){
      digitalWrite(ledPins[j], HIGH);
    }
    delay(200);
    resetLights();
    delay(200);
  }
}

void flashLevelPassed(){
  for(int x=0;x<5;x++){
    resetLights();
    for(int i=0;i<4;i++){
      digitalWrite(ledPins[i], HIGH);
      delay(100);
      digitalWrite(ledPins[i], LOW);
    }
  }
}

void logData(int currentValue, int currentIndex){
  Serial.print("currentValue ==");
  Serial.println(currentValue);
  Serial.print("currentIndex ==");
  Serial.println(currentIndex);
  Serial.print("data[currentIndex] ==");
  Serial.println(sequence.get(currentIndex));
}

