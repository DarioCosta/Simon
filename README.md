Il progetto che abbiamo pensato di realizzare per mettere in pratica le prime nozioni di elettronica ed informatica applicata alla piattaforma Arduino è la riproposizione di un “gioco elettronico musicale” che ha conosciuto un periodo di grande popolarità negli anni ottanta: Simon.


Se il nome non vi dice niente... 

![Simon immagine](doc/simon.jpg)

https://it.wikipedia.org/wiki/Simon_(gioco)

>>>
Simon è un gioco elettronico della Milton Bradley, inventato da Ralph Baer (creatore anche della prima console per videogiochi, la Magnavox Odyssey del 1972) e Howard J. Morrison[1]. Fu lanciato negli Stati Uniti nel 1978 e successivamente diffuso in tutto il mondo, conquistando un enorme successo e diventando un simbolo della cultura pop negli anni ottanta.
Il gioco è una sorta di variante elettronica del gioco per bambini noto nel mondo anglosassone come Simon says. "Simon" si presenta come un disco con quattro grandi pulsanti di colore rosso, blu, verde e giallo. Questi pulsanti si illuminano secondo una sequenza casuale; all'illuminazione di ciascun pulsante è associata anche l'emissione di una determinata nota musicale. Una volta terminata la sequenza, il giocatore deve ripeterla premendo i pulsanti nello stesso ordine. Se riesce in questo compito il giocatore si vede proporre una nuova sequenza, uguale alla precedente con l'aggiunta di nuovo pulsante/tono; la sequenza da ripetere diventa quindi sempre più lunga e il compito del giocatore più difficile, comunque per ricordare il colore di sequenze, a partire già dalla prima sequenza, si può scrivere su un foglio il nome del colore della sequenza e di quelle che si aggiungeranno o segnare con un pennarello o pastello del rispettivo colore ma in questo modo il gioco sarebbe ovviamente più facile.
Il Simon originale non è più in produzione. Hasbro commercializza alcune varianti, tra cui un Simon da viaggio e un Simon evoluto (chiamato Simon Trickster) con diverse possibili modalità di gioco (inclusa una in cui i quadranti non appaiono colorati).
>>>

È un classico che i primissimi esperimenti con Arduino siano legati a interruttori e luci che si accendono e spengono. E allora sarà stato tutto quel armeggiare con i led colorati che ci ha fatto tornare in mente i ricordi di gioventù e ci ha fatto venire voglia di provare a ricostruire quel gioco elettronico d’altri tempi.


La nostra versione non deve essere per forza identica all’originale, quindi l’idea di base è molto semplice: prendiamo quattro led colorati, quattro interruttori “push button”, un piezo, una manciata di fili e resistenze, scriviamo lo sketch, lo carichiamo sul ATMega ed è fatta… e che ce vo’?
In effetti il progetto dovrebbe essere alla portata di due principianti volenterosi alle prime armi. Ci mettiamo all’opera e cerchiamo di documentare le varie fasi del progetto nel WIKI di progetto.

https://gitlab.com/DarioCosta/Simon/wikis/home
